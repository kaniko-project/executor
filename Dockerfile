# https://github.com/GoogleContainerTools/kaniko/issues/1881#issuecomment-1019386728
ARG DOCKER_HUB_URL
FROM ${DOCKER_HUB_URL}alpine:edge as alpine_to_apk_add

# We could try doing this the other way around:
# https://github.com/WoozyMasta/kaniko-tools/blob/master/Dockerfile
# https://github.com/ubatalov/kaniko-curl/blob/master/Dockerfile

ARG CI_SERVER_HOST

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
# However, the whole point of using alpine as the base is that it can.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN pwd && \
    set -o allexport && \
    . ./fix_all_gotchas.sh && \
    set +o allexport && \
    apk add --no-cache wget && \
    # apk add --no-cache libssl1.1 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/ && \
    # apk add --no-cache libssl3 && \
    apk add --no-cache ca-certificates && \
    apk add --no-cache musl-utils && \
    echo "ldd /usr/bin/wget" && \
    ldd /usr/bin/wget && \
    echo "md5sum /lib/ld-musl-x86_64.so.1" && \
    md5sum /lib/ld-musl-x86_64.so.1 && \
    # So we can use a file /etc/ld-musl-x86_64.path https://www.musl-libc.org/doc/1.1.24/manual.html
    (cat /etc/ld-musl-x86_64.path || echo "/etc/ld-musl-x86_64.path is not present.") && \
    printf "/kaniko/lib:/kaniko/usr/lib:/lib:/usr/local/lib:/usr/lib" > /etc/ld-musl-x86_64.path && \
    mkdir /kaniko/usr && \
    mkdir /kaniko/lib && \
    mkdir /kaniko/usr/lib && \
    ldd /usr/bin/wget && \
    cat /etc/wgetrc && \
    ls /usr/share/ca-certificates/mozilla/ && \
    # (ls /etc/ssl1.1 || echo "/etc/ssl1.1 does not exist.")
    ( (ls /etc/ssl && echo "/etc/ssl found!") || echo "/etc/ssl does not exist.")
    # ls /etc/ssl1.1/cert.pem

RUN cat /usr/share/ca-certificates/mozilla/* > /ca-certificates.crt
RUN echo "ca-certificate=/kaniko/ssl/certs/ca-certificates.crt" > /.wgetrc
RUN echo "ca-directory=/etc/ca-certificates/update.d/" >> /.wgetrc

RUN echo "We will need each of the shared libraries used by wget. ldd tells us which libraries we need." && \
    ldd /usr/bin/wget && \
    cp /lib/ld-musl-x86_64.so.1 /kaniko/lib/ld-musl-x86_64.so.1 && \
    cp /lib/libc.musl-x86_64.so.1 /kaniko/lib/libc.musl-x86_64.so.1 && \
    # mv /lib/libcrypto.so.1.1 /kaniko/lib/libcrypto.so.1.1 && \
    mv /lib/libcrypto.so.3 /kaniko/lib/libcrypto.so.3 && \
    # mv /lib/libssl.so.1.1 /kaniko/lib/libssl.so.1.1 && \
    # mv /usr/lib/libssl.so.1.1 /kaniko/usr/lib/libssl.so.1.1 && \
    mv /lib/libssl.so.3 /kaniko/lib/libssl.so.3 && \
    cp /lib/libz.so.1 /kaniko/lib/libz.so.1 && \
    cp /usr/lib/libpcre2-8.so.0 /kaniko/usr/lib/libpcre2-8.so.0 && \
    cp /usr/lib/libidn2.so.0 /kaniko/usr/lib/libidn2.so.0 && \
    cp /usr/lib/libunistring.so.5 /kaniko/usr/lib/libunistring.so.5
RUN ldd /usr/bin/wget

RUN mkdir /kaniko/usr/bin && \
    mv /usr/bin/wget /kaniko/usr/bin && \
    ldd /kaniko/usr/bin/wget && \
    PATH="/kaniko/usr/bin:${PATH}" wget https://$CI_SERVER_HOST && \
    set -o allexport && \
    . ./fix_all_gotchas.sh && \
    set +o allexport && \
    PATH="/kaniko/usr/bin:${PATH}" wget https://gitlab.com

FROM gcr.io/kaniko-project/executor:debug

ENV wget_location=/kaniko/usr/bin/wget

# Copy files from Kaniko image
COPY --from=alpine_to_apk_add /kaniko/usr/bin/wget $wget_location
RUN ["/busybox/sh", "-c", "rm /busybox/wget || echo '/busybox/wget not found.'"]
# COPY --from=alpine_to_apk_add /etc/wgetrc /etc/wgetrc
# Putting it in /kaniko/etc/wgetrc has no effect, and putting it in /etc/wgetrc puts it into images built.

#RUN ["/busybox/mkdir", "-p", "/lib"]

ENV LD_LIBRARY_PATH=/kaniko/lib:/kaniko/usr/lib:${LD_LIBRARY_PATH}
COPY --from=alpine_to_apk_add /etc/ld-musl-x86_64.path /kaniko/etc/ld-musl-x86_64.path
COPY --from=alpine_to_apk_add /lib/ld-musl-x86_64.so.1 /kaniko/lib/ld-musl-x86_64.so.1
COPY --from=alpine_to_apk_add /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
# Currently, /kaniko/lib/ld-musl-x86_64.so.1 is ignored for unknown reasons, so (for example) we still have no TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 unless we also put it at /lib/ld-musl-x86_64.so.1.
# Interestingly, although ../etc/ld-musl-$(ARCH).path is looked for relative to /lib/ld-musl-x86_64.so.1 per https://www.musl-libc.org/doc/1.1.24/manual.html , it seems to be found at /kaniko/etc/ld-musl-x86_64.path without needing to put it at /etc/ld-musl-x86_64.path. But I'm not totally sure whether it's really being found or whether the other libraries are found some other way.

COPY --from=alpine_to_apk_add /lib/libc.musl-x86_64.so.1 /kaniko/lib/libc.musl-x86_64.so.1
#COPY --from=alpine_to_apk_add /etc/ssl1.1/cert.pem /etc/ssl1.1/cert.pem
#COPY --from=alpine_to_apk_add /etc/ssl1.1/certs /etc/ssl1.1/certs
#COPY --from=alpine_to_apk_add /etc/ssl1.1/ct_log_list.cnf /etc/ssl1.1/ct_log_list.cnf
#COPY --from=alpine_to_apk_add /etc/ssl1.1/ct_log_list.cnf.dist /etc/ssl1.1/ct_log_list.cnf.dist
#COPY --from=alpine_to_apk_add /etc/ssl1.1/openssl.cnf /etc/ssl1.1/openssl.cnf
#COPY --from=alpine_to_apk_add /etc/ssl1.1/openssl.cnf.dist /etc/ssl1.1/openssl.cnf.dist
# COPY --from=alpine_to_apk_add /kaniko/lib/libcrypto.so.1.1 /kaniko/lib/libcrypto.so.1.1
COPY --from=alpine_to_apk_add /kaniko/lib/libcrypto.so.3 /kaniko/lib/libcrypto.so.3
COPY --from=alpine_to_apk_add /kaniko/lib/libz.so.1 /kaniko/lib/libz.so.1
# COPY --from=alpine_to_apk_add /usr/lib/engines-1.1/afalg.so /usr/lib/engines-1.1/afalg.so
# COPY --from=alpine_to_apk_add /usr/lib/engines-1.1/capi.so /usr/lib/engines-1.1/capi.so
# COPY --from=alpine_to_apk_add /usr/lib/engines-1.1/padlock.so /usr/lib/engines-1.1/padlock.so
# COPY --from=alpine_to_apk_add /usr/lib/engines-3/afalg.so /usr/lib/engines-3/afalg.so
# COPY --from=alpine_to_apk_add /usr/lib/engines-3/capi.so /usr/lib/engines-3/capi.so
# COPY --from=alpine_to_apk_add /usr/lib/engines-3/padlock.so /usr/lib/engines-3/padlock.so
# COPY --from=alpine_to_apk_add /usr/lib/libcrypto.so.1.1 /usr/lib/libcrypto.so.1.1
COPY --from=alpine_to_apk_add /usr/lib/libcrypto.so.3 /usr/lib/libcrypto.so.3
COPY --from=alpine_to_apk_add /kaniko/usr/lib/libunistring.so.5 /kaniko/usr/lib/libunistring.so.5
#COPY --from=alpine_to_apk_add /usr/bin/idn2 /usr/bin/idn2
COPY --from=alpine_to_apk_add /kaniko/usr/lib/libidn2.so.0 /kaniko/usr/lib/libidn2.so.0
COPY --from=alpine_to_apk_add /kaniko/usr/lib/libpcre2-8.so.0 /kaniko/usr/lib/libpcre2-8.so.0
# COPY --from=alpine_to_apk_add /kaniko/lib/libssl.so.1.1 /kaniko/lib/libssl.so.1.1
COPY --from=alpine_to_apk_add /kaniko/lib/libssl.so.3 /kaniko/lib/libssl.so.3
# COPY --from=alpine_to_apk_add /kaniko/usr/lib/libssl.so.1.1 /usr/lib/libssl.so.1.1

# https://github.com/GoogleContainerTools/kaniko/blob/main/deploy/Dockerfile_debug#L68
# COPY --from=certs /ca-certificates.crt /kaniko/ssl/certs/
# https://www.happyassassin.net/posts/2015/01/12/a-note-about-ssltls-trusted-certificate-stores-and-platforms/
COPY --from=alpine_to_apk_add /usr/share/ca-certificates/mozilla/ /kaniko/ssl/certs/
COPY --from=alpine_to_apk_add /usr/share/ca-certificates/mozilla/* /kaniko/ssl/certs/
#COPY --from=alpine_to_apk_add /usr/share/ca-certificates/mozilla/ /usr/share/ca-certificates/mozilla
# don't mess with /certs since that can mess with built images
#COPY --from=alpine_to_apk_add /usr/share/ca-certificates/mozilla/* /certs/
#COPY --from=alpine_to_apk_add /usr/share/ca-certificates/mozilla/* /usr/local/ssl/
COPY --from=alpine_to_apk_add /ca-certificates.crt /kaniko/ssl/certs/ca-certificates.crt
#COPY --from=alpine_to_apk_add /ca-certificates.crt /etc/ssl/certs
# /etc/ssl/cert.pem appears to be the only thing that actually matters.
# I can't find this documented anywhere, but apparently on busybox /etc/ssl/cert.pem is what matters.
# https://www.reddit.com/r/AlpineLinux/comments/bbnx5t/trusted_certificates_for_wget_busybox/
# busybox uses only a separate store: /etc/ssl/cert.pem
# Replacing /etc/ssl/cert.pem with a symbolic link to /etc/ssl/certs/ca-certificates.crt
COPY --from=alpine_to_apk_add /ca-certificates.crt /etc/ssl/cert.pem

COPY --from=alpine_to_apk_add /etc/ssl/certs /etc/ssl/certs
COPY --from=alpine_to_apk_add /etc/ssl/certs /kaniko/etc/ssl/certs

COPY --from=alpine_to_apk_add /etc/apk/protected_paths.d/ca-certificates.list /etc/apk/protected_paths.d/ca-certificates.list
COPY --from=alpine_to_apk_add /etc/ca-certificates.conf /etc/ca-certificates.conf
COPY --from=alpine_to_apk_add /etc/ca-certificates/update.d/certhash /etc/ca-certificates/update.d/certhash

#COPY --from=alpine_to_apk_add /.wgetrc $HOME/.wgetrc
ENV SSL_CERT_DIR=/kaniko/ssl/certs

#RUN pwd

#RUN ls $wget_location

#ENV PATH="/kaniko/usr/bin:${PATH}"
# https://github.com/GoogleContainerTools/kaniko/blob/main/deploy/Dockerfile_debug#L73
ENV PATH /usr/local/bin:/kaniko:/kaniko/usr/bin:/busybox
