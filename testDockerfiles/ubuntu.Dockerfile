FROM ubuntu

ARG CI_SERVER_HOST

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $ETC_ENVIRONMENT_LOCATION ./environment.sh
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport && \
    . ./fix_all_gotchas.sh && \
    set +o allexport && \
    apt-get update && \
    apt-get install --assume-yes curl && \
    apt-get install --assume-yes wget && \
    wget https://$CI_SERVER_HOST && rm index.html && \
    wget https://gitlab.com && rm index.html && \
    wget https://gradle.org && rm index.html && \
    . ./cleanup.sh
