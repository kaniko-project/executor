FROM quay.io/centos/centos:stream

ARG CI_SERVER_HOST

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN curl --help && \
    command -v curl && \
    set -o allexport && \
    . ./fix_all_gotchas.sh && \
    set +o allexport && \
    # sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-Linux-* && \
    (cat $HOME/.curlrc || echo "$HOME/.curlrc not found.") && \
    ((curl https://mirrors.fedora.org --output index.html && rm index.html) || echo "curl https://mirrors.fedora.org failed.") && \
    dnf install --assumeyes epel-release && \
    dnf install --assumeyes git && \
    dnf install --assumeyes wget && \
    which wget && \
    (ldd --help || echo "ldd not found.") && \
    ldd /usr/bin/wget && \
    wget https://$CI_SERVER_HOST && rm index.html && \
    wget https://gitlab.com && rm index.html && \
    wget https://gradle.org && rm index.html && \
    wget https://services.gradle.org && rm index.html && \
    . ./cleanup.sh
