ARG DOCKER_HUB_URL
ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=pythonpackagesonalpine
ARG DOCKER_BASE_IMAGE_NAME=basic-python-packages-pre-installed-on-alpine
ARG DOCKER_BASE_IMAGE_TAG=pip-alpine
FROM ${DOCKER_HUB_URL}alpine:edge

ARG CI_SERVER_HOST

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
# But with Alpine, we should be fine.
# ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
# ADD $CLEANUP_SCRIPT_LOCATION .

RUN wget --proxy off $FIX_ALL_GOTCHAS_SCRIPT_LOCATION \
    && wget --proxy off $CLEANUP_SCRIPT_LOCATION \
    && set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && . ./cleanup.sh
